import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { ContentComponent } from './components/content/content.component';
import { MainComponent } from './components/main/main.component';
import { LoginLogoutDirective } from './directives/login-logout.directive';
import { SortAlphabetPipe } from './pipes/sort-alphabet.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ContentComponent,
    SidebarComponent,
    MainComponent,
    LoginLogoutDirective,
    SortAlphabetPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
