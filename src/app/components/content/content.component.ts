import { Component, Input } from '@angular/core';
import { subjectType } from '../sidebar/sidebar.component';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent {
  @Input() subject_list: subjectType = {
    subjectName: 'Angular',
    description: `Angular is an open framework and
    platform for creating Single Page Applications, written in TypeScript and supported and developed by Google.`,
    thumbnail: '../../../assets/images/angular.png',
  }

  staticContent = [
    {
      content:'Dynamic reports and dashboard',
      thumbnail:'../../../assets/images/checkbox.png'
    },
    {
      content:'Templates for everyone',
      thumbnail:'../../../assets/images/checkbox.png'
    },
    {
      content:'Development workflow',
      thumbnail:'../../../assets/images/checkbox.png'
    },
    {
      content:'Limitles business automation',
      thumbnail:'../../../assets/images/checkbox.png'
    },
  ]
}
