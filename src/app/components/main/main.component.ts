import { Component } from '@angular/core';
import { subjectType } from '../sidebar/sidebar.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent {
  subject_list: subjectType = {
    subjectName: 'Angular',
    description: `Angular is an open framework and
    platform for creating Single Page Applications, written in TypeScript and supported and developed by Google.`,
    thumbnail: '../../../assets/images/angular.png',
  };
  receiveFromSidebar(subject: subjectType) {
    this.subject_list = subject;
  }
}
