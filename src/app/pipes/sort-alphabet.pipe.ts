import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortAlphabet'
})
export class SortAlphabetPipe implements PipeTransform {
  transform(array: any[], propertyName: string): any[] {
    if (!array || array.length <= 1) {
      return array;
    }
    return array.sort((a, b) => {
      const propertyA = a[propertyName].toLowerCase();
      const propertyB = b[propertyName].toLowerCase();
      if (propertyA < propertyB) {
        return -1;
      } else if (propertyA > propertyB) {
        return 1;
      } else {
        return 0;
      }
    });
  }

}
